angular.module("avanceApp").factory("PageService", function($http, AvanceConfig) {

    function listAllPages(fn) {
        $http.get(AvanceConfig.restUrl + "/page/all").then(function (data) {
            for(let i = 0; i < data.data.length; i++) {
                data.data[i].creationString = new Date(data.data[i].creation).toDateString()
            }
            fn(data.data, data);
        })
    }

    function searchPages(str, fn) {
        listAllPages(function(data, obj) {
            let filteredPages = [];
            for(let page of data) {
                console.log(JSON.stringify(page).indexOf(str), str);
                if(JSON.stringify(page).indexOf(str) != -1) {
                    filteredPages.push(page);
                }
            }
            console.log(filteredPages);
            fn(filteredPages, obj);
        })
    }

    function createPage(page, fn) {
        $http.post(AvanceConfig.restUrl + "/page/create", {
            "pagetitle" : page.pagetitle,
            "content" : page.content,
            "project" : page.projectAvanceId
        }).then(function(data) {
            fn(data.data, data);
        })
    }

    function updatePage(page, fn) {
        $http.post(AvanceConfig.restUrl+ "/page/update", {
            "pagetitle" : page.pagetitle,
            "content" : page.content,
            "project" : page.projectAvanceId,
            "avanceid" : page.avanceid
        }).then(function(data) {
            fn(data.data, data);
        });
    }

    function deletePage(avanceid, fn) {
         $http.get(AvanceConfig.restUrl + "/page/delete/" + avanceid).then(function(data) {
             fn(data.data, data);
         })
    }

    
    return {
        listAllPages,
        searchPages,
        createPage,
        updatePage,
        deletePage
    };
});