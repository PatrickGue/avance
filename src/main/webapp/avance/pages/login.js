angular.module("avanceApp").controller("LoginController", function($scope, $window, AuthService, UserService) {
    $scope.login = function () {
        UserService.login($scope.username, $scope.password, 
            function() {

            },
            function() {
                
            }
        );
        $scope.$parent.loggedin = true;
        $window.location = "#!/home";
    };
});