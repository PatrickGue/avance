package ch.patrickguenthard.avance.rest.resources;

import ch.patrickguenthard.avance.logic.ResourceManager;
import ch.patrickguenthard.avance.logic.interf.ProjectManager;
import ch.patrickguenthard.avance.model.Project;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/project")
public class ProjectRest {

    private ProjectManager projectManager;

    public ProjectRest() {
        projectManager = ResourceManager.getProjectManager();
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllProjects() {
        List<Map<String, Object>> list = new ArrayList<>();

        for(Project project : projectManager.getAllProjects()) {
            Map<String, Object> projectMap = new HashMap<>();
            projectMap.put("projectname", project.getProjectName());
            projectMap.put("key", project.getKey());
            projectMap.put("description", project.getProjectDescription());
            projectMap.put("creation", project.getProjectCreation());
            projectMap.put("avanceid", project.getAvanceId());
            list.add(projectMap);
        }

        Gson g = new Gson();

        return g.toJson(list);
    }

    @GET
    @Path("{avanceid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getProject(@PathParam("avanceid") String avanceid) {
        Project project = projectManager.getProject(avanceid);
        Map<String, Object> projectMap = new HashMap<>();
        projectMap.put("projectname", project.getProjectName());
        projectMap.put("key", project.getKey());
        projectMap.put("description", project.getProjectDescription());
        projectMap.put("creation", project.getProjectCreation());
        projectMap.put("avanceid", project.getAvanceId());

        return new Gson().toJson(projectMap);
    }

    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    public String createProject(String json) {
        Map<String, Object> map = new Gson().fromJson(json, Map.class);
        Project project = new Project();
        project.setKey((String) map.get("key"));
        project.setProjectName((String) map.get("projectname"));
        project.setProjectDescription((String) map.get("description"));
        String avanceId = projectManager.createProject(project);
        return getProject(avanceId);
    }
}
