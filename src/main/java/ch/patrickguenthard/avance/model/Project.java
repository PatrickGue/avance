package ch.patrickguenthard.avance.model;

import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Proxy(lazy=false)
@Table(name = "AVC_PROJECT")
public class Project extends AvanceEntity {

    @Id
    @Column(name = "project_id", nullable = true, unique = true)
    private Integer projectId;

    @Column(name = "project_key", nullable = true)
    private String key;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "project_description")
    private String projectDescription;

    @Column(name = "project_creation")
    private Date projectCreation;

    /*@OneToMany(
        mappedBy = "project_id"
    )
    private List<Page> pages;*/
}
