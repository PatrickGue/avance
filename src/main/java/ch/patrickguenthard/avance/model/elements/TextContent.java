package ch.patrickguenthard.avance.model.elements;

import lombok.Data;

@Data
public class TextContent extends  Content {

    private String htmlText;
    private String markupText;

}
