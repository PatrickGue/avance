package ch.patrickguenthard.avance.model.issue;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "AVC_ISSUE_STATUS")
public class IssueStatus{
    @Id
    @Column(name = "status_id")
    private Integer statusId;

    @Column(name = "status_name")
    private String  statusName;
}

