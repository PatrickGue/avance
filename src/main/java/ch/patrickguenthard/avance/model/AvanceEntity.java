package ch.patrickguenthard.avance.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class AvanceEntity {

    private static final String possibleChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_";
    private static final int keyLength = 8;

    @Column(name = "avance_id")
    private String avanceId;

    public void generateAvanceId() {
        StringBuilder newId= new StringBuilder();
        for(int i = 0; i < keyLength; i++) {
            int num = (int) Math.floor(Math.random() * possibleChars.length());
            newId.append(possibleChars.charAt(num));
        }

        this.avanceId = newId.toString();
    }
}
