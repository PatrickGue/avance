package ch.patrickguenthard.avance.persistence;

import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaQuery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class HibernateFetchDataIntegrationTest {
    private Session session;
    private EntityManager entityManager;

    @Before
    public void init() {
        session = HibernateSession.getSession();
        entityManager = session.getEntityManagerFactory().createEntityManager();
    }

    @Test
    @Ignore //TODO rewrite test with JPA
    public void getUsers() {
        entityManager.getTransaction().begin();

        List<User> result = entityManager.createQuery( "from User", User.class ).getResultList();
        for ( User event : result ) {
            System.out.println( "Event (" + event.getCreation() + ") : " + event.getUserName() );
        }

        Project proj = entityManager.createQuery("from Project", Project.class).getSingleResult();
        //System.out.println(proj.toString() + "\n" + proj.getPages().toString());
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
