angular.module("avanceApp").controller("ProjectsController", function($scope, $http, ProjectService, PageService) {
    $scope.projectList = [];
    $scope.selectedProject = undefined;
    $scope.listAllProjects = function() {
        ProjectService.listAllProjects(function(data) {
            $scope.projectList = data;
        });
    };

    $scope.listAllProjects();

    $scope.selectProject = function(index) {
        $scope.selectedProject = $scope.projectList[index];
        PageService.searchPages($scope.selectedProject.avanceid, function(data) {
            $scope.selectedProject.pages = data;
        })
    }
});