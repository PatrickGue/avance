package ch.patrickguenthard.avance.model;

import ch.patrickguenthard.avance.model.elements.TextContent;
import ch.patrickguenthard.avance.model.issue.IssueStatus;
import ch.patrickguenthard.avance.model.issue.IssueType;
import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Proxy(lazy=false)
@Table(name = "AVC_ISSUE")
public class Issue extends AvanceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "issue_id", nullable = true, unique = true)
    private Integer issueId;

    @Column(name = "issue_title", nullable = true, unique = true)
    private String title;

    @Column(name = "issue_description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "issue_project_id")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "issue_type_id")
    private IssueType issueType;

    @ManyToOne
    @JoinColumn(name = "issue_status_id")
    private IssueStatus issueStatus;

    @Column(name = "issue_creation")
    private Date issueCreation;

    @Column(name = "issue_updated")
    private Date issueUpdated;

    /*@Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="markupText", column = @Column(name="issue_content") ),
    } )
    private TextContent description;*/
}
