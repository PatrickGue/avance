package ch.patrickguenthard.avance.rest.resources;

import ch.patrickguenthard.avance.logic.ResourceManager;
import ch.patrickguenthard.avance.logic.interf.IssueManager;
import ch.patrickguenthard.avance.model.Issue;
import ch.patrickguenthard.avance.model.issue.IssueStatus;
import ch.patrickguenthard.avance.model.issue.IssueType;
import com.google.gson.Gson;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("issue")
public class IssueRest {

    private IssueManager issueManager;

    public IssueRest() {
        issueManager = ResourceManager.getIssueManager();
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllIssues() {
        List<Map<String, Object>> list = new ArrayList<>();
        for(Issue issue : issueManager.getAllIssue()) {
            Map<String, Object> map = new HashMap<>();
            map.put("issuetitle", issue.getTitle());
            map.put("description", issue.getDescription());
            map.put("creation", issue.getIssueCreation().getTime());
            map.put("updated", issue.getIssueUpdated().getTime());
            map.put("avanceid", issue.getAvanceId());
            map.put("type", issue.getIssueType());
            map.put("status", issue.getIssueStatus().getStatusId());
            list.add(map);
        }
        return new Gson().toJson(list);
    }

    @GET
    @Path("/status/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllIssueStatus() {
        List<Map<String, Object>> list = new ArrayList<>();
        for(IssueStatus status : issueManager.getAllIssueStatus()) {
            Map<String, Object> map = new HashMap<>();
            map.put("status", status.getStatusName());
            map.put("id", status.getStatusId());
            list.add(map);
        }
        return new Gson().toJson(list);
    }

    @GET
    @Path("/type/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllIssueTypes() {
        List<Map<String, Object>> list = new ArrayList<>();
        for(IssueType status : issueManager.getAllIssueTypes()) {
            Map<String, Object> map = new HashMap<>();
            map.put("type", status.getTypeName());
            map.put("id", status.getTypeId());
            list.add(map);
        }
        return new Gson().toJson(list);
    }
}
