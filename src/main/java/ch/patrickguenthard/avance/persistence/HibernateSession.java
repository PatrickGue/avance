package ch.patrickguenthard.avance.persistence;

import ch.patrickguenthard.avance.model.*;
import ch.patrickguenthard.avance.model.issue.IssueStatus;
import ch.patrickguenthard.avance.model.issue.IssueType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateSession {

    private static Session session;

    public static Session getSession() {
        if(session == null) {
            initSessionJavaConfig();
        }
        return session;

    }

    private static void initSessionJavaConfig() {
        try {
            Configuration configuration = new Configuration();

            //Create Properties, can be read from property files too
            Properties props = new Properties();
            props.put("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
            props.put("hibernate.connection.url", "jdbc:mysql://localhost/avance?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
            props.put("hibernate.connection.username", "root");
            props.put("hibernate.connection.password", "root1234");
            props.put("hibernate.current_session_context_class", "thread");
            props.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");

            configuration.setProperties(props);

            configuration.addAnnotatedClass(User.class);
            configuration.addAnnotatedClass(Group.class);
            configuration.addAnnotatedClass(Project.class);
            configuration.addAnnotatedClass(Issue.class);
            configuration.addAnnotatedClass(Page.class);
            configuration.addAnnotatedClass(IssueStatus.class);
            configuration.addAnnotatedClass(IssueType.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            session = sessionFactory.openSession();
        }
        catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
