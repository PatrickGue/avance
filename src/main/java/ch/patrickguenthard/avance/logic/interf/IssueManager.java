package ch.patrickguenthard.avance.logic.interf;

import ch.patrickguenthard.avance.model.Issue;
import ch.patrickguenthard.avance.model.Page;
import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.issue.IssueStatus;
import ch.patrickguenthard.avance.model.issue.IssueType;

import java.util.List;

public interface IssueManager {
    /**
     * Get all issues
     * @return list of all pages
     */
    public List<Issue> getAllIssue();

    /**
     * Get issues of a Project
     * @param project
     * @return list all pages of a project
     */
    public List<Issue> getIssueByProject(Project project);

    /**
     * Save issue to database
     * @param newIssue
     */
    public void updateIssue(Issue newIssue);

    /**
     * Create new Page of a page
     * @param proj
     * @param issue
     * @return new Page
     */
    public Issue createIssue(Project proj, Issue issue);

    /**
     * Create new Page based on a Issue
     * @param page
     * @return new Issue
     */
    public Issue createIssueFromPage(Page page);

    public List<IssueType> getAllIssueTypes();

    public List<IssueStatus> getAllIssueStatus();
}
