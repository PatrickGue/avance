package ch.patrickguenthard.avance.model.elements;

public class Attachment {
    private String attachmentName;
    private String url;
    private long size;
}
