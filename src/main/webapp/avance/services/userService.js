angular.module("avanceApp").factory("UserService", function($http, AvanceConfig) {

    function listAllUsers(fn) {
        $http.get(AvanceConfig.restUrl + "/user/all").then(function (data) {
            for(let i = 0; i < data.data.length; i++) {
                data.data[i].creationString = new Date(data.data[i].creation).toDateString()
            }
            fn(data.data, data);
        })
    }

    function searchUsers(str, fn) {
        listAllUsers(function(data, obj) {
            let filteredUsers = [];
            for(user of data) {
                if(JSON.stringify(user).indexOf(str) != -1) {
                    filteredUsers.push(user);
                }
            }
            fn(filteredUsers, obj);
        })
    }

    function login(username, password, ok, error) {
        $http.post(AvanceConfig.restUrl + "/user/login", {
            "username" : username,
            "password" : password
        }).then(
            function(data) {
                ok(data.data,data)
            },
            function(data) {
                error(data.data,data);
            }
        );
    }
    
    return {
        listAllUsers,
        searchUsers
    };
});