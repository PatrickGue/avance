/*
    MYSQL
*/

DROP DATABASE IF EXISTS avance;
CREATE DATABASE avance;

USE avance;

CREATE TABLE AVC_USER (
    user_id         INTEGER NOT NULL AUTO_INCREMENT,
    user_name       VARCHAR(255) NOT NULL,
    user_full_name  VARCHAR(255) NOT NULL,
    user_password   VARCHAR(255) NOT NULL,
    user_creation   DATETIME DEFAULT CURRENT_TIMESTAMP,
    avance_id       VARCHAR(65) NULL,

    PRIMARY         KEY(user_id)
);

CREATE TABLE AVC_GROUP (
    group_id        INTEGER NOT NULL AUTO_INCREMENT,
    group_name      VARCHAR(255) NOT NULL,
    avance_id       VARCHAR(65) NULL,

    PRIMARY         KEY(group_id)
);

CREATE TABLE AVC_USER_GROUP (
    user_group_id   INTEGER NOT NULL AUTO_INCREMENT,
    user_id         INTEGER NOT NULL,
    group_id        INTEGER NOT NULL,
    avance_id       VARCHAR(65) NULL,

    PRIMARY KEY(user_group_id)
);

CREATE TABLE AVC_PROJECT (
    project_id          INTEGER NOT NULL AUTO_INCREMENT,
    project_key         VARCHAR(64) NOT NULL,
    project_name        VARCHAR(255) NOT NULL,
    project_description TEXT NOT NULL,
    project_creation    DATETIME DEFAULT CURRENT_TIMESTAMP,
    avance_id           VARCHAR(65) NULL,

    PRIMARY             KEY(project_id)
);

/*CREATE TABLE AVC_PERMISSION (
    group_project_id INTEGER NOT NULL AUTO_INCREMENT,
    project_id
);*/

CREATE TABLE AVC_ISSUE (
    issue_id                INTEGER NOT NULL AUTO_INCREMENT,
    issue_number            INTEGER NOT NULL,
    issue_title             VARCHAR(1024) NOT NULL,
    issue_description       TEXT NOT NULL,
    issue_project_id        INTEGER NOT NULL,
    issue_creator_user_id   INTEGER NOT NULL,
    issue_assignee_user_id  INTEGER,
    issue_status_id         INTEGER NOT NULL,
    issue_type_id           INTEGER NOT NULL,
    issue_creation          DATETIME DEFAULT CURRENT_TIMESTAMP,
    issue_updated           DATETIME DEFAULT CURRENT_TIMESTAMP,
    avance_id               VARCHAR(65) NULL,

    PRIMARY                 KEY(issue_id)
);

CREATE TABLE AVC_ISSUE_STATUS (
    status_id       INTEGER NOT NULL AUTO_INCREMENT,
    status_name     VARCHAR(64) NOT NULL,

    PRIMARY KEY(status_id)
);

CREATE TABLE AVC_ISSUE_TYPE (
    type_id       INTEGER NOT NULL AUTO_INCREMENT,
    type_name     VARCHAR(64) NOT NULL,

    PRIMARY KEY(type_id)
);

CREATE TABLE AVC_PAGE (
    page_id INTEGER NOT NULL AUTO_INCREMENT,
    page_title VARCHAR(1024) NOT NULL,
    page_content TEXT NOT NULL,
    project_id INTEGER NOT NULL,
    page_creation DATETIME DEFAULT CURRENT_TIMESTAMP,
    page_updated DATETIME NULL,
    avance_id VARCHAR(65) NULL,

    PRIMARY KEY(page_id)
);