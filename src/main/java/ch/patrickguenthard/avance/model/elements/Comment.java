package ch.patrickguenthard.avance.model.elements;

import ch.patrickguenthard.avance.model.User;
import lombok.Data;

@Data
public class Comment {
    private TextContent commentText;
    private User creator;
}
