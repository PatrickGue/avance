package ch.patrickguenthard.avance.model;

import lombok.Data;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Data
@Entity
@Table(name = "AVC_USER")
@Transactional
public class User extends AvanceEntity {

    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer userId;

    @Column(name = "user_name", unique = true, nullable = true)
    private String userName;

    @Column(name = "user_full_name", unique = true, nullable = true)
    private String fullName;

    @Column(name = "user_password", unique = true, nullable = true)
    private String password;

    @Column(name = "user_creation", unique = true, nullable = true)
    private Date creation;

    @ManyToMany
    @JoinTable(
            name = "AVC_USER_GROUP",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "group_id") }
    )
    private List<Group> groups;


    public static User createUser() {
        User user = new User();
        user.generateAvanceId();
        return user;
    }

    public UserSimple getUserSimple() {
        return new UserSimple(this.userName, this.getAvanceId());
    }

    @Data
    public static class UserSimple {
        private String username;
        private String avanceId;
        public UserSimple(String username, String avanceId) {
            this.username = username;
            this.avanceId = avanceId;
        }
    }
}
