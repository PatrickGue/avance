use avance;

INSERT INTO AVC_ISSUE_TYPE(type_id, type_name) VALUES(1, "Task");
INSERT INTO AVC_ISSUE_TYPE(type_id, type_name) VALUES(2, "Bug");

INSERT INTO AVC_ISSUE_STATUS(status_id, status_name) values (1, "New");
INSERT INTO AVC_ISSUE_STATUS(status_id, status_name) values (2, "In Progress");
INSERT INTO AVC_ISSUE_STATUS(status_id, status_name) values (3, "Done");