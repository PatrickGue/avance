package ch.patrickguenthard.avance.model.elements;

import ch.patrickguenthard.avance.model.User;
import lombok.Data;

import java.util.List;

@Data
public abstract class Content {
    private List<Attachment> attachments;
    private User creator;
}
