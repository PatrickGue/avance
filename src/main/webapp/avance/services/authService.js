angular.module("avanceApp").factory("AuthService", function() {
    function saveToken(token) {
        window.localStorage.setItem("token", token);
    }

    function getToken(token) {
        window.localStorage.getItem("token");
    }

    function hasToken() {
        window.localStorage.getItem(token) != undefined;
    }

    return {
        saveToken,
        getToken,
        hasToken
    };
});