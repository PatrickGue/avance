package ch.patrickguenthard.avance.logic.interf;

import ch.patrickguenthard.avance.model.Group;
import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.User;

import java.util.List;

public interface PermissionManager {
    public boolean isAllowed(User user, Project project);

    public void setAllowed(User user, Project project);

    public void revokeAllowed(User user, Project project);

    public List<Group> getAllGroups();
}
