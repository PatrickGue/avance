package ch.patrickguenthard.avance.model.issue;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "AVC_ISSUE_TYPE")
public class IssueType {
    @Id
    @Column(name = "type_id")
    private Integer typeId;

    @Column(name = "type_name")
    private String  typeName;
}
