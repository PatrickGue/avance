angular.module("avanceApp").factory("MarkupService", function() {
    const E = {
        "HEADING" : "h1:",
        "SUBHEADING" : "h2",
        "SECTION" : "h3",
        "PANEL_START" : "panel {",
        "PANEL_END" : "} panel",
        "TABLE_START" : "table {",
        "TABLE_END" : "} table",
        "TABLE_ELM" : "|",
        "LINK" : "[]"
    };
    
    function markupToHTML(markup) {
        let lines = markup.split('\n');
        let html = "";
        for(line of lines) {
            if(line.indexOf(E.HEADING) == 0) {
                html += "<h1>" + line.substr(4) + "</h1>";
            }
            else if(line.indexOf(E.SUBHEADING) == 0) {
                html += "<h2>" + line.substr(4) + "</h2>";
            }
            else if(line.indexOf(E.SECTION) == 0) {
                html += "<h3>" + line.substr(4) + "</h3>";
            }
            else if(line.indexOf(E.PANEL_START) == 0) {
                html += "<div class=\"panel\">";
            }
            else if(line.indexOf(E.PANEL_END) == 0) {
                html += "</div>";
            }
            else if(line.indexOf(E.TABLE_START) == 0) {
                html += "<table>";
            }
            else if(line.indexOf(E.TABLE_END) == 0) {
                html += "</table>";
            }
            else if(line.indexOf(E.TABLE_ELM) == 0) {
                html += line.replaceAll("| ", "<tr><td>").replaceAll(" | ", "<td></td>").replaceAll(" |","</td></tr>");
            }
            else {
                html += "<p>" + line + "</p>";
            }
        }

        return html;
    }

    return {
        markupToHTML
    }
});