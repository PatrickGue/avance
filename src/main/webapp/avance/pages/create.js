angular.module("avanceApp").controller("CreateController", function($scope, ProjectService, PageService) {
    $scope.createIssueOrPage = 'issue';

    $scope.page = {
        projects : []
    };

    ProjectService.listAllProjects(function(data) {
        $scope.page.projects = data;
    });



    $scope.setCreateIssueOrPage = function(iop) {
        $scope.createIssueOrPage = iop;
    };

    $scope.isCreateIssueOrPage = function(iop) {
        return iop == $scope.createIssueOrPage;
    };


});