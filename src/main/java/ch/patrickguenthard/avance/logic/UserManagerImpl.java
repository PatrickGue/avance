package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.UserManager;
import ch.patrickguenthard.avance.model.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class UserManagerImpl extends ManagerImpl implements UserManager {

    private UserManagerImpl() {}

    @Override
    public List<User> getAllUser() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<User> userList = q.getResultList();
        for(User user: userList) {
            if(user.getAvanceId() == null) {
                user.generateAvanceId();
                updateUser(user);
            }
        }
        return userList;
    }

    @Override
    public void updateUser(User user) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public User getUserByUsername(String username) {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("userName"), username));
        Query q = entityManager.createQuery(criteria);
        User user = (User) q.getSingleResult();
        if(user.getAvanceId() == null) {
            user.generateAvanceId();
            updateUser(user);
        }

        return user;
    }
}
