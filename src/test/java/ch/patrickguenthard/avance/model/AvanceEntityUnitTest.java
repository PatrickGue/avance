package ch.patrickguenthard.avance.model;

import org.junit.Test;
import static org.junit.Assert.*;

public class AvanceEntityUnitTest {
    @Test
    public void testIdGeneration() {
        User user = new User();
        user.generateAvanceId();
        assertEquals(user.getAvanceId().length(),8);
    }
}
