package ch.patrickguenthard.avance.persistence;

import org.hibernate.Session;
import org.junit.Test;
import static org.junit.Assert.*;

public class HibernateSessionIntegrationTest {

    @Test
    public void createSession() {
        Session session = HibernateSession.getSession();
        assertNotNull(session);
    }
}
