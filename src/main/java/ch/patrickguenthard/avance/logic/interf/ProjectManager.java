package ch.patrickguenthard.avance.logic.interf;

import ch.patrickguenthard.avance.model.Project;

import java.util.List;

public interface ProjectManager {
    public List<Project> getAllProjects();

    public Project getProject(String avanceId);

    public void updateProject(Project project);

    /**
     *
     * @param project
     * @return AvanceId
     */
    public String createProject(Project project);

}
