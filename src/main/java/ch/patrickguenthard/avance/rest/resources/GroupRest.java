package ch.patrickguenthard.avance.rest.resources;

import ch.patrickguenthard.avance.logic.ResourceManager;
import ch.patrickguenthard.avance.logic.interf.PermissionManager;
import ch.patrickguenthard.avance.model.Group;
import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.User;
import com.google.gson.Gson;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("permission")
public class GroupRest {

    private PermissionManager permissionManager;

    public GroupRest() {
        permissionManager = ResourceManager.getPermissionManager();
    }

    @GET
    @Path("group/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllGroups() {
        List<Map<String, Object>> list = new ArrayList<>();

        for(Group group : permissionManager.getAllGroups()) {
            Map<String, Object> groupMap = new HashMap<>();
            groupMap.put("groupname", group.getGroupName());
            groupMap.put("avanceid",group.getAvanceId());
            groupMap.put("users", new ArrayList<User.UserSimple>());
            for(User user : group.getUsers()) {
                ((List)groupMap.get("users")).add(user.getUserSimple());
            }
            list.add(groupMap);
        }

        Gson g = new Gson();

        return g.toJson(list);
    }
}
