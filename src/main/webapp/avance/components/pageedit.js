(function() {
    function PageEditController(ProjectService, PageService) {
        let self = this;
        
        ProjectService.listAllProjects(function(data) {
            self.projects = data;
        });

        self.createPage = function() {

            PageService.createPage({
                "pagetitle" : self.page.pagetitle,
                "content": self.page.content,
                "projectAvanceId": self.page.project
            }, function(data) {
                console.log(data);
                self.page.avanceid = data.avanceid;
            });

            self.done();
        };

        self.updatePage = function() {
            PageService.updatePage({
                "avanceid" : self.page.avanceid,
                "pagetitle" : self.page.pagetitle,
                "content" : self.page.content,
                "projectAvanceId" : self.page.project
            }, function(data) {
                self.done(data);
            });

        };



    }

    angular.module("avanceApp").component("pageEdit", {
        bindings: {
            page: "=",
            type: "<",
            done: "=",
            cancel: "="
        },
        controller: PageEditController,
        templateUrl: "avance/components/pageedit.html"
    });
})();