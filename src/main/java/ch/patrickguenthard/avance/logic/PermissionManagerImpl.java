package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.PermissionManager;
import ch.patrickguenthard.avance.model.Group;
import ch.patrickguenthard.avance.model.Page;
import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.User;
import org.hibernate.cfg.NotYetImplementedException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class PermissionManagerImpl extends ManagerImpl implements PermissionManager {
    @Override
    public boolean isAllowed(User user, Project project) {
        throw new NotYetImplementedException();
    }

    @Override
    public void setAllowed(User user, Project project) {
        throw new NotYetImplementedException();
    }

    @Override
    public void revokeAllowed(User user, Project project) {
        throw new NotYetImplementedException();
    }

    @Override
    public List<Group> getAllGroups() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteria = builder.createQuery(Group.class);
        Root<Group> root = criteria.from(Group.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<Group> groupList = q.getResultList();
        return groupList;
    }
}
