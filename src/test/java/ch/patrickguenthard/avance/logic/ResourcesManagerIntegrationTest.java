package ch.patrickguenthard.avance.logic;


import static org.junit.Assert.*;

import ch.patrickguenthard.avance.logic.interf.UserManager;
import ch.patrickguenthard.avance.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.List;

public class ResourcesManagerIntegrationTest {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Test
    public void getManager() {
        UserManager manager = ResourceManager.getUserManager();
        assertNotNull(manager);
        List<User> users = manager.getAllUser();
        assertTrue(users.size() > 0);
        for(User user : users) {
            logger.info(user.getGroups());
        }
    }
}
