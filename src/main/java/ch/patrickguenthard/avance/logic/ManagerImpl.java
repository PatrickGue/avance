package ch.patrickguenthard.avance.logic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public abstract class ManagerImpl {
    private EntityManagerFactory entityManagerFactory;

    protected EntityManager requestEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
