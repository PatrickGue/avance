package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.ProjectManager;
import ch.patrickguenthard.avance.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProjectManagerImpl extends ManagerImpl implements ProjectManager {
    @Override
    public List<Project> getAllProjects() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteria = builder.createQuery(Project.class);
        Root<Project> root = criteria.from(Project.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<Project> projectList = q.getResultList();
        for(Project project : projectList) {
            if(project.getAvanceId() == null) {
                project.generateAvanceId();
                updateProject(project);
            }
        }
        return projectList;
    }

    @Override
    public Project getProject(String avanceId) {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteria = builder.createQuery(Project.class);
        Root<Project> root = criteria.from(Project.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("avanceId"), avanceId));

        Query q = entityManager.createQuery(criteria);
        Project project = (Project) q.getSingleResult();
        return project;
    }

    @Override
    public void updateProject(Project project) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public String createProject(Project project) {
        project.generateAvanceId();
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(project);
        entityManager.getTransaction().commit();
        return project.getAvanceId();

    }
}
