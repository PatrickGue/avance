package ch.patrickguenthard.avance.rest.resources;

import ch.patrickguenthard.avance.logic.ResourceManager;
import ch.patrickguenthard.avance.logic.interf.PageManager;
import ch.patrickguenthard.avance.logic.interf.ProjectManager;
import ch.patrickguenthard.avance.model.Page;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("page")
public class PageRest {

    private PageManager pageManager;
    private ProjectManager projectManager;

    public PageRest() {
        pageManager = ResourceManager.getPageManager();
        projectManager = ResourceManager.getProjectManager();
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllPages() {
        List<Map> pages = new ArrayList<>();
        for(Page page : pageManager.getAllPages()) {
            Map<String, Object> map = new HashMap<>();
            map.put("pagetitle", page.getPageTitle());
            map.put("content", page.getContent());
            map.put("creation", page.getPageCreation().getTime());
            map.put("avanceid", page.getAvanceId());
            map.put("project", page.getProject().getAvanceId());
            pages.add(map);
        }
        return new Gson().toJson(pages);
    }

    @GET
    @Path("/{avanceid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSinglePage(@PathParam("avanceid") String avanceId) {
        Page page = pageManager.getPageById(avanceId);
        Map<String, Object> map = new HashMap<>();
        map.put("pagetitle", page.getPageTitle());
        map.put("content", page.getContent());
        map.put("creation", page.getPageCreation().getTime());
        map.put("avanceid", page.getAvanceId());
        map.put("project", page.getProject().getAvanceId());
        return new Gson().toJson(map);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public String createPage(String json) {
        Map<String, Object> obj = new Gson().fromJson(json, Map.class);
        Page page = new Page();
        page.setPageTitle((String) obj.get("pagetitle"));
        page.setContent((String) obj.get("content"));
        page.setProject(projectManager.getProject((String) obj.get("project")));
        page.setPageCreation(new Date());
        String avanceId = pageManager.createPage(page);
        return getSinglePage(avanceId);
    }


    @POST
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public String updatePage(String json) {
        Map<String, Object> obj = new Gson().fromJson(json, Map.class);
        Page page = pageManager.getPageById((String) obj.get("avanceid"));
        page.setPageTitle((String) obj.get("pagetitle"));
        page.setContent((String) obj.get("content"));
        page.setProject(projectManager.getProject((String) obj.get("project")));
        page.setPageUpdated(new Date());
        pageManager.updatePage(page);
        Map<String, String> returnMap = new HashMap<>();
        returnMap.put("message", "Page update successful");
        returnMap.put("avanceid", page.getAvanceId());
        return new Gson().toJson(returnMap);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/delete/{avanceid}")
    public String deletePage(@PathParam("avanceid") String avanceid) {
        Page page = pageManager.getPageById(avanceid);
        pageManager.deletePage(page);
        return "{\"message\": \"deletion successful\"}";
    }
}
