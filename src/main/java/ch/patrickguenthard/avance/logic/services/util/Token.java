package ch.patrickguenthard.avance.logic.services.util;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
public class Token {

    private static final String availableChars = "0123456789ABCDEF";

    private String token;
    private Date expiration;

    /*
    TODO: Implement secure algorythm
     */
    public Token() {
        expiration = new Date(new Date().getTime() + 1000l);
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < /* TODO: properties length */ 32; i++) {
            builder.append(availableChars.charAt((int)Math.floor(Math.random() * availableChars.length())));
        }
        token = builder.toString();
    }
}
