angular.module("avanceApp").controller("AdminController", function($scope, UserService) {
    $scope.userList = [];

    $scope.selectedUser = undefined;

    $scope.listAllUsers = function() {
        UserService.listAllUsers(function(data) {
            $scope.userList = data;
        });
    };

    $scope.selectUser = function(index) {
        $scope.selectedUser = $scope.userList[index];
    }

    $scope.listAllUsers();
});