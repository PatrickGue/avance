package ch.patrickguenthard.avance.logic.exceptions;

public class ResourceNotFoundeException extends Exception {
    public ResourceNotFoundeException() {
        super();
    }

    public ResourceNotFoundeException(String message) {
        super(message);
    }

    public ResourceNotFoundeException(Throwable throwable) {
        super(throwable);
    }

    public ResourceNotFoundeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
