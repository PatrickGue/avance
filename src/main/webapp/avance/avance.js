String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

const app = angular.module("avanceApp",["ngRoute","ngSanitize"]);

app.controller("AvanceController", function($scope, $window) {
    $scope.reader = false;
    $scope.loggedin = true;
    //$scope.loggedin = false;
    //$window.location = "#!/login"
    $scope.back = function () {
        $window.history.back();
    };
    $scope.forward = function () {
        $window.history.forward();
    };
});

app.constant("AvanceConfig", {
    "restUrl": "/api/rest"
});

app.config(function($routeProvider) {
    $routeProvider
    .when("/home", {
        templateUrl : "avance/pages/home.html",
        controller: "HomeController"
    })
    .when("/menu", {
        templateUrl : "avance/pages/menu.html"
    })
    .when("/demo", {
        templateUrl : "avance/pages/demo.html"
    })
    .when("/admin", {
        templateUrl : "avance/pages/admin.html",
        controller: "AdminController"
    })
    .when("/login", {
        templateUrl: "avance/pages/login.html",
        controller: "LoginController"
    })
    .when("/projects", {
        templateUrl: "avance/pages/projects.html",
        controller: "ProjectsController"
    })
    .when("/page/:pageId", {
        templateUrl: "avance/pages/page.html",
        controller: "PageController"
    })
    .when("/create", {
        templateUrl: "avance/pages/create.html",
        controller: "CreateController"
    })
    .otherwise({
        redirectTo: "/home"
    });
});