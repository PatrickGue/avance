package ch.patrickguenthard.avance.logic.services.util;

import ch.patrickguenthard.avance.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Login {
    private User user;
    private Token token;
}
