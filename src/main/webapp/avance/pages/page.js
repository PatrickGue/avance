angular.module("avanceApp").controller("PageController", function($scope, $http, $routeParams, MarkupService, PageService) {
    $scope.page = undefined;
    $scope.reading = false;
    $scope.loadPage = function() {
        $http.get("/api/rest/page/" + $routeParams.pageId).then(function(data) {
            $scope.page = data.data;
        });
    };

    $scope.loadPage();
    

    $scope.cancelEdit = function() {
        $scope.edit = false;
        $scope.loadPage();
    }

    $scope.doneUpdate = function() {
        $scope.edit = false;
    }

    $scope.markupToHtml = function(content) {
        return MarkupService.markupToHTML(content ? content : "");
    };

    $scope.deletePage = function() {
        if(window.confirm("Do you really want to delete this page?")) {
            PageService.deletePage($scope.page.avanceid, function() {
                $scope.$parent.back()
            });
        }
    }
});