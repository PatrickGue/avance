$(document).ready(function() {
    $(document).on("scroll", function() {
        if($(document).scrollTop() > 22 ) {
            $("header").addClass("scroll");
            $("meta[name=theme-color]").attr("content", "#000000");
        }
        else {
            $("header").removeClass("scroll");
            $("meta[name=theme-color]").attr("content", "#ee5500");
        }
    });

    [].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
        img.setAttribute('src', img.getAttribute('data-src'));
        img.onload = function() {
            img.removeAttribute('data-src');
        };
    });
});