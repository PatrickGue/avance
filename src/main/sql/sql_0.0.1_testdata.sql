use avance;

INSERT INTO AVC_USER(user_id, user_name, user_full_name, user_password) VALUES(1,"test","Test User","password");
INSERT INTO AVC_USER(user_id, user_name, user_full_name, user_password) VALUES(2,"avance","Avance Test User","password");

INSERT INTO AVC_GROUP(group_id, group_name) VALUES(1, "avance-default");

INSERT INTO AVC_USER_GROUP (user_group_id, user_id, group_id) VALUES(1,1,1);

INSERT INTO AVC_PROJECT (project_id, project_key, project_name, project_description) VALUES(1,"TEST", "Test Project", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in blandit eros. Cras dapibus mi vel purus tristique volutpat. Cras a mauris a augue ullamcorper dignissim. Maecenas nulla nisl, laoreet id diam ut, sollicitudin luctus nisl. Suspendisse auctor efficitur eros imperdiet placerat. Proin tincidunt urna vel arcu volutpat luctus. Pellentesque in purus vitae elit faucibus sollicitudin sed et elit. Praesent ultricies elit id enim auctor, id feugiat dolor posuere. Mauris posuere ultricies orci, ac luctus lacus vehicula at.");
INSERT INTO AVC_PROJECT (project_id, project_key, project_name, project_description) VALUES(2,"AVC", "Avance Development", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in blandit eros. Cras dapibus mi vel purus tristique volutpat. Cras a mauris a augue ullamcorper dignissim. Maecenas nulla nisl, laoreet id diam ut, sollicitudin luctus nisl. Suspendisse auctor efficitur eros imperdiet placerat. Proin tincidunt urna vel arcu volutpat luctus. Pellentesque in purus vitae elit faucibus sollicitudin sed et elit. Praesent ultricies elit id enim auctor, id feugiat dolor posuere. Mauris posuere ultricies orci, ac luctus lacus vehicula at.");

INSERT INTO AVC_ISSUE(issue_id, issue_number, issue_title, issue_description, issue_project_id, issue_creator_user_id, issue_type_id, issue_status_id) VALUES(1, 1, "First Issue", "Descr", 1, 1, 1, 1);

INSERT INTO AVC_PAGE(page_id, page_title, page_content, project_id, avance_id) VALUES(2,"Avance Test Page", "h1: Avance Test\nh2: Test\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque in blandit eros. Cras dapibus mi vel purus tristique volutpat. Cras a mauris a augue ullamcorper dignissim. Maecenas nulla nisl, laoreet id diam ut, sollicitudin luctus nisl.\npanel {\nSuspendisse auctor efficitur eros imperdiet placerat. Proin tincidunt urna vel arcu volutpat luctus. Pellentesque in purus vitae elit faucibus sollicitudin sed et elit. Praesent ultricies elit id enim auctor, id feugiat dolor posuere. Mauris posuere ultricies orci, ac luctus lacus vehicula at.\n} panel", 1, "abcd1234");