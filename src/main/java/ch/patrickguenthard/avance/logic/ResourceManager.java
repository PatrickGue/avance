package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.*;
import ch.patrickguenthard.avance.persistence.HibernateSession;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class ResourceManager {
    private static Logger logger = LogManager.getLogger(ResourceManager.class);

    private ResourceManager() {}

    private static IssueManager issueManager;
    private static PageManager pageManager;
    private static ProjectManager projectManager;
    private static UserManager userManager;
    private static PermissionManager permissionManager;

    private static Session session;
    private static EntityManagerFactory managerFactory;

    static {
        session = HibernateSession.getSession();
        managerFactory = session.getEntityManagerFactory();
        try {
            initIssueManager();
            initPageManager();
            initProjectManager();
            initUserManager();
            initPermissionManager();
        } catch (Exception e) {
            logger.info("Init fails");
            logger.error(e);
        }
    }

    public static IssueManager getIssueManager() {
        return issueManager;
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    public static ProjectManager getProjectManager() {
        return projectManager;
    }

    public static UserManager getUserManager() {
        return userManager;
    }

    public static PermissionManager getPermissionManager() {
        return permissionManager;
    }

    private static void initIssueManager() throws Exception{
        Constructor<IssueManagerImpl> constructor;
        constructor = IssueManagerImpl.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        issueManager = (IssueManagerImpl) constructor.newInstance();
        Field field = issueManager.getClass().getSuperclass().getDeclaredField("entityManagerFactory");

        field.setAccessible(true);
        field.set(issueManager, managerFactory);

    }

    private static void initPageManager() throws Exception{
        Constructor<PageManagerImpl> constructor;
        constructor = PageManagerImpl.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        pageManager = (PageManagerImpl) constructor.newInstance();
        Field field = pageManager.getClass().getSuperclass().getDeclaredField("entityManagerFactory");

        field.setAccessible(true);
        field.set(pageManager, managerFactory);

    }

    private static void initProjectManager() throws Exception{
        Constructor<ProjectManagerImpl> constructor;
        constructor = ProjectManagerImpl.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        projectManager = (ProjectManagerImpl) constructor.newInstance();
        Field field = projectManager.getClass().getSuperclass().getDeclaredField("entityManagerFactory");

        field.setAccessible(true);
        field.set(projectManager, managerFactory);

    }

    private static void initUserManager() throws Exception{
        Constructor<UserManagerImpl> constructor;
        constructor = UserManagerImpl.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        userManager = (UserManagerImpl) constructor.newInstance();
        Field field = projectManager.getClass().getSuperclass().getDeclaredField("entityManagerFactory");

        field.setAccessible(true);
        field.set(userManager, managerFactory);

    }

    private static void initPermissionManager() throws Exception{
        Constructor<PermissionManagerImpl> constructor;
        constructor = PermissionManagerImpl.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        permissionManager = (PermissionManagerImpl) constructor.newInstance();
        Field field = permissionManager.getClass().getSuperclass().getDeclaredField("entityManagerFactory");

        field.setAccessible(true);
        field.set(permissionManager, managerFactory);

    }



}
