package ch.patrickguenthard.avance.rest.resources;

import ch.patrickguenthard.avance.logic.ResourceManager;
import ch.patrickguenthard.avance.logic.interf.UserManager;
import ch.patrickguenthard.avance.logic.services.AuthService;
import ch.patrickguenthard.avance.model.Group;
import ch.patrickguenthard.avance.model.User;
import com.google.gson.Gson;
import org.hibernate.cfg.NotYetImplementedException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("/user")
public class UserRest {

    private UserManager userManager;

    public UserRest() {
        userManager = ResourceManager.getUserManager();
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public String listAllUsers() {
        List<Map> list = new ArrayList<>();
        for(User user : userManager.getAllUser()) {
            Map<String, Object> map = new HashMap<>();
            map.put("username",user.getUserName());
            map.put("fullname",user.getFullName());
            map.put("avanceid",user.getAvanceId());
            map.put("creation", user.getCreation().getTime());
            map.put("groups",new ArrayList<String>());
            for(Group group : user.getGroups()) {
                ((List)map.get("groups")).add(group.getGroupName());
            }
            list.add(map);
        }
        Gson g = new Gson();
        return g.toJson(list);
    }

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public String login(String json) throws Exception{
        Map<String, String> map = new Gson().fromJson(json, Map.class);
        String userName = map.get("username");
        String plainPassword = map.get("password");
        String hashPassword = AuthService.hashPassword(plainPassword);
        User user = userManager.getUserByUsername(userName);
        if(user.getPassword().equals(hashPassword)) {
            Map<String, String> returnMap = new HashMap<>();
            returnMap.put("token", AuthService.generateToken(user));
            return new Gson().toJson(returnMap);
        }
        else {
            throw new NotAuthorizedException("Login failed");
        }
    }
}
