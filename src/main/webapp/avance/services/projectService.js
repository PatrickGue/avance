angular.module("avanceApp").factory("ProjectService", function($http, AvanceConfig) {

    function listAllProjects(fn) {
        $http.get(AvanceConfig.restUrl + "/project/all").then(function (data) {
            for(let i = 0; i < data.data.length; i++) {
                data.data[i].creationString = new Date(data.data[i].creation).toDateString()
            }
            fn(data.data, data);
        })
    }

    function searchProjects(str, fn) {
        listAllProjects(function(data, obj) {
            let filteredProjects = [];
            for(let project of data) {
                console.log(JSON.stringify(project).indexOf(str), str);
                if(JSON.stringify(project).indexOf(str) != -1) {
                    filteredProjects.push(project);
                }
            }
            fn(filteredProjects, obj);
        })
    }

    function createProject(project, fn) {
        $http.post(AvanceConfig.restUrl + "/project/create").then(function(data) {
            fn(data.data, data);
        });
    }

    
    return {
        listAllProjects,
        searchProjects,
        createProject
    };
});