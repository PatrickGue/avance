package ch.patrickguenthard.avance.model;

import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Data
@Entity
@Proxy(lazy=false)
@Table(name = "AVC_GROUP")
public class Group extends AvanceEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id", unique = true, nullable = true)
    private Integer groupId;

    @Column(name = "group_name", unique = true, nullable = true)
    private String groupName;

    @JoinTable(
            name = "AVC_USER_GROUP",
            joinColumns = { @JoinColumn(name = "group_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    @ManyToMany
    private List<User> users;
}
