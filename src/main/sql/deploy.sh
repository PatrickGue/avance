#!/bin/bash

USER=root
PASS=root1234

cat sql_0.0.1_create.sql | mysql -u$USER -p$PASS -v
cat sql_0.0.1_defaults.sql | mysql -u$USER -p$PASS -v
cat sql_0.0.1_testdata.sql | mysql -u$USER -p$PASS -v