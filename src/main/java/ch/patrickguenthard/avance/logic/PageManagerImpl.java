package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.PageManager;
import ch.patrickguenthard.avance.model.Issue;
import ch.patrickguenthard.avance.model.Page;
import ch.patrickguenthard.avance.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class PageManagerImpl extends ManagerImpl implements PageManager {

    private PageManagerImpl() {}

    @Override
    public List<Page> getAllPages() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Page> criteria = builder.createQuery(Page.class);
        Root<Page> root = criteria.from(Page.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<Page> pageList = q.getResultList();
        for(Page page : pageList) {
            if(page.getAvanceId() == null) {
                page.generateAvanceId();
                updatePage(page);
            }
        }
        return pageList;
    }

    @Override
    public List<Page> getPagesByProject(Project project) {
        return null;
    }

    @Override
    public void updatePage(Page page) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(page);
        entityManager.getTransaction().commit();
    }


    @Override
    public String createPage(Page page) {
        EntityManager entityManager = requestEntityManager();
        if(page.getAvanceId() == null) {
            page.generateAvanceId();
        }
        entityManager.getTransaction().begin();
        entityManager.persist(page);
        entityManager.getTransaction().commit();
        return page.getAvanceId();
    }

    @Override
    public String createPageFromIssue(Issue issue) {
        return null;
    }

    @Override
    public Page getPageById(String avanceId) {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Page> criteria = builder.createQuery(Page.class);
        Root<Page> root = criteria.from(Page.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("avanceId"), avanceId));

        Query q = entityManager.createQuery(criteria);
        Page page = (Page) q.getSingleResult();
        if(page.getAvanceId() == null) {
            page.generateAvanceId();
            updatePage(page);
        }
        return page;
    }

    @Override
    public void deletePage(Page page) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.remove(page);
        entityManager.getTransaction().commit();
    }
}
