package ch.patrickguenthard.avance.logic;

import ch.patrickguenthard.avance.logic.interf.IssueManager;
import ch.patrickguenthard.avance.model.Issue;
import ch.patrickguenthard.avance.model.Page;
import ch.patrickguenthard.avance.model.Project;
import ch.patrickguenthard.avance.model.issue.IssueStatus;
import ch.patrickguenthard.avance.model.issue.IssueType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class IssueManagerImpl extends ManagerImpl implements IssueManager {

    private static Logger logger = LogManager.getLogger(IssueManagerImpl.class);

    /**
     * IssueManagerImpl can be fetched via ResourceManager
     */
    private IssueManagerImpl() {}

    @Override
    public List<Issue> getAllIssue() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Issue> criteria = builder.createQuery(Issue.class);
        Root<Issue> root = criteria.from(Issue.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<Issue> issueList = q.getResultList();
        for(Issue issue : issueList) {
            if(issue.getAvanceId() == null) {
                issue.generateAvanceId();
                updateIssue(issue);
            }
        }
        return issueList;
    }

    @Override
    public List<Issue> getIssueByProject(Project project) {
        EntityManager entityManager = requestEntityManager();

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Issue> criteria = builder.createQuery(Issue.class);
        Root<Issue> root = criteria.from(Issue.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("project"), project));
        Query q = entityManager.createQuery(criteria);
        List<Issue> issueList = q.getResultList();
        for(Issue issue : issueList) {
            if(issue.getAvanceId() == null) {
                issue.generateAvanceId();
                updateIssue(issue);
            }
        }
        return issueList;
    }

    @Override
    public void updateIssue(Issue newIssue) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(newIssue);
        entityManager.close();
    }

    @Override
    public Issue createIssue(Project proj, Issue issue) {
        EntityManager entityManager = requestEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(issue);
        entityManager.getTransaction().commit();
        return issue;
    }

    @Override
    public Issue createIssueFromPage(Page page) {
        return null;
    }

    @Override
    public List<IssueType> getAllIssueTypes() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IssueType> criteria = builder.createQuery(IssueType.class);
        Root<IssueType> root = criteria.from(IssueType.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<IssueType> issueTypeList = q.getResultList();
        return issueTypeList;
    }

    @Override
    public List<IssueStatus> getAllIssueStatus() {
        EntityManager entityManager = requestEntityManager();
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IssueStatus> criteria = builder.createQuery(IssueStatus.class);
        Root<IssueStatus> root = criteria.from(IssueStatus.class);
        criteria.select(root);
        Query q = entityManager.createQuery(criteria);
        List<IssueStatus> issueStatusList = q.getResultList();
        return issueStatusList;
    }
}
