angular.module("avanceApp").factory("IssueService", function($http, AvanceConfig) {

    function listAllIssues(fn) {
        $http.get(AvanceConfig.restUrl + "/issue/all").then(function (data) {
            for(let i = 0; i < data.data.length; i++) {
                data.data[i].creationString = new Date(data.data[i].creation).toDateString()
            }
            fn(data.data, data);
        })
    }

    function searchIssues(str, fn) {
        listAllIssues(function(data, obj) {
            let filteredIssues = [];
            for(user of data) {
                if(JSON.stringify(user).indexOf(str) != -1) {
                    filteredIssues.push(user);
                }
            }
            fn(filteredIssues, obj);
        })
    }

    function listAllIssueStatus(fn) {
        $http.get(AvanceConfig.restUrl + "/issue/status/all").then(function(data) {
            fn(data.data, data);
        });
    }
    
    return {
        listAllIssues,
        searchIssues,
        listAllIssueStatus
    };
});