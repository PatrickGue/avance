package ch.patrickguenthard.avance.logic.services;

import ch.patrickguenthard.avance.logic.services.util.Login;
import ch.patrickguenthard.avance.model.User;
import org.hibernate.cfg.NotYetImplementedException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class AuthService {

    private static List<Login> logins;

    /**
     * Static classes only
     */
    private AuthService() {}

    public static String hashPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(password.getBytes("UTF-8"));
        byte[] hash = digest.digest();
        return byteArrayToHex(hash);
    }

    private static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static String generateToken(User user) {
        throw new NotYetImplementedException();
    }

}
