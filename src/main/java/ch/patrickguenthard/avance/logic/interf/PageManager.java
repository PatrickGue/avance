package ch.patrickguenthard.avance.logic.interf;

import ch.patrickguenthard.avance.model.Issue;
import ch.patrickguenthard.avance.model.Page;
import ch.patrickguenthard.avance.model.Project;

import java.util.List;

public interface PageManager {
    /**
     * Get all pages
     *
     * @return list of all pages
     */
    public List<Page> getAllPages();

    /**
     * Get pages of a Project
     *
     * @param project
     * @return list all pages of a project
     */
    public List<Page> getPagesByProject(Project project);

    /**
     * Save page to database
     *
     * @param newPage
     */
    public void updatePage(Page newPage);

    /**
     * Create new Page of a page
     *
     * @param page
     * @return new Page
     */
    public String createPage(Page page);


    /**
     * Create new Page based on a Issue
     *
     * @param issue
     * @return new Page
     */
    public String createPageFromIssue(Issue issue);


    public Page getPageById(String avanceId);

    public void deletePage(Page page);
}
