package ch.patrickguenthard.avance.model;

import ch.patrickguenthard.avance.model.elements.TextContent;
import lombok.Data;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Proxy(lazy=false)
@Table(name = "AVC_PAGE")
public class Page extends AvanceEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "page_id", unique = true, nullable = true)
    private Integer pageId;

    @Column(name = "page_title")
    private String pageTitle;

    @Column(name = "page_content")
    private String content;
    //private TextContent content;

    @Column(name = "page_creation")
    private Date pageCreation;

    @Column(name = "page_updated")
    private Date pageUpdated;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
