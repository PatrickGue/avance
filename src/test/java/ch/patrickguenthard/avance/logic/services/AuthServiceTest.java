package ch.patrickguenthard.avance.logic.services;

import org.junit.Test;
import static org.junit.Assert.*;

public class AuthServiceTest {

    @Test
    public void testHashPassword() throws Exception{
        String password = "password";
        String expectedHash = "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8";

        String calculatedHash = AuthService.hashPassword(password);

        assertEquals(expectedHash, calculatedHash);

    }
}
