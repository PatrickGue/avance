package ch.patrickguenthard.avance.logic.interf;

import ch.patrickguenthard.avance.model.User;

import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import java.util.List;

public interface UserManager {
    public List<User> getAllUser();

    public void updateUser(User user);

    public User getUserByUsername(String username);
}
